<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
class informationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $rajib= DB::table('info')->join('reg', 'info.id','reg.students_id')->select('info.*', 'reg.reg')->get();
       return view('admin.student.index', compact('rajib'));
    }

    public function pdf(){

        // $data = DB::table('info')->get();
        // $pdf = PDF::loadView('admin.pdfview', compact('data'));
  
        // return $pdf->download('hello.pdf');

         // $items = DB::table("info")->get();
         //    $pdf = PDF::loadView('admin.pdfview');
         //    return $pdf->download('pdfview.pdf');
       
        $data = DB::table('users')->get();
        $pdf = PDF::loadView('admin.pdfview', compact('data'));
  
        return $pdf->download('itsolutionstuff.pdf');
    }


        
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //echo "hello Bangladesh";
        return view('admin.student.information');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validatedData = $request->validate([
             'firstName' => 'required|max:25|min:3',
             'lastName' => 'required|max:25|min:3',
             'email' => 'required|unique:students',
    ]);

           $data=array();
           $data['firstName']=$request->firstName;
           $data['lastName']=$request->lastName;
           $data['email']=$request->email;

           $catagory=DB::table('info')->insert($data);
           if ($catagory) {
             
               return redirect()->route('info')->with('success', 'data store successfully');
           }
           else{
            
            return redirect()->back('/')->with('failed', 'data store failed');
           }
    }

    public function registation(){
        
        $data=DB::table('info')->get();
       
        return view ('admin.student.registation', compact('data'));
    }

    public function regstore(Request $request)
    {
         $validatedData = $request->validate([
             'students_id' => 'required',
             'reg' => 'required|max:25|min:3',
            
    ]);

         $data=array();
           $data['students_id']=$request->students_id;
           $data['reg']=$request->reg;
        

           $catagory=DB::table('reg')->insert($data);
           if ($catagory) {
             
               return redirect()->route('reg')->with('success', 'data store successfully');
           }
           else{
            
            return redirect()->back('/')->with('failed', 'data store failed');
           }


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
