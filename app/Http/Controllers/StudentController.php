<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $shahaza=DB::table('students')->get();
        // return view('admin.students',compact('shahaza'));
        //return view('admin.students');

       $prodip= DB::table('students')->get();
       return view('admin.students', compact('prodip'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $validatedData = $request->validate([
             'firstName' => 'required|max:25|min:3',
             'lastName' => 'required|max:25|min:3',
             'email' => 'required|unique:students',
    ]);

           $data=array();
           $data['firstName']=$request->firstName;
           $data['lastName']=$request->lastName;
           $data['email']=$request->email;
           $data['created_at']= date('m/d/Y h:i:s a');

           $catagory=DB::table('students')->insert($data);
           if ($catagory) {
             $notification = array(
                'message' => 'infromation Store Successfully',
                'alert-type' => 'success'
            );
               return redirect()->route('all')->with($notification);
           }
           else{
            $notification = array(
                'message' => 'infromation Store Failed',
                'alert-type' => 'error'
            );
            return redirect()->back('/')->with($notification);
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit= DB::table('students')->first();
       return view('admin.edit')->with('edit',$edit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
             'firstName' => 'required|max:25|min:3',
             'lastName' => 'required|max:25|min:3',
             'email' => 'required',
    ]);

           $data=array();
           $data['firstName']=$request->firstName;
           $data['lastName']=$request->lastName;
           $data['email']=$request->email;

           $update=DB::table('students')->where('id',$id)->update($data);
           if ($update) {
                $notification = array(
                'message' => 'infromation Update successfully!',
                'alert-type' => 'success'
            );
           return redirect()->route('all')->with($notification);
           }
           else{
            $notification = array(
                'message' => 'infromation failed successfully!',
                'alert-type' => 'error'
            );
           return redirect()->route('all')->with($notification);
           }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=DB::table('students')->where('id',$id)->delete();
        if ($delete) {
           $notification = array(
                'message' => 'infromation Deleted successfully!',
                'alert-type' => 'success'
            );
           return redirect()->route('all')->with('success', 'infromation Deleted successfully!');
        }
        else{
             $notification = array(
                'message' => 'infromation Delete Failed!',
                'alert-type' => 'error'
            );
           return redirect()->route('all')->with('success', 'infromation Delete Failed!');
        }
    }
}
