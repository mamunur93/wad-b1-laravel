<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rajib=DB::table('teachers')->get();
        return view('admin.teacher.prodip', compact('rajib'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $validatedData = $request->validate([
             'firstName' => 'required|max:255',
             'lastName' => 'required',
             'email' => 'required',
             'gender' => 'required',
             'city' => 'required',
             'image' => 'required|image|mimes:jpg,jpeg,png,gif',
    ]);

        $shahazada=$request->hobby;
// var_dump($hobby) ;
        $hobby=implode(',', $shahazada);

        $data=array();
         $data['firstName']=$request->firstName;
         $data['lastName']=$request->lastName;
         $data['email']=$request->email;
         $data['gender']=$request->gender;
         $data['hobby']=$hobby;
         $data['city']=$request->city;
         //$data['image']=$request->image;

         $img=$request->file('image');
         // print_r($img);

         $img_name=hexdec(uniqid());
         $ext=strtolower($img->getClientOriginalExtension());
         $img_full_name=$img_name.'.'.$ext;
         $location="upload/";
         $image_url=$location.$img_full_name;
         $success=$img->move($location,$img_full_name);

         $data['image']=$image_url;
         $sabbir= DB::table('teachers')->insert($data);
         if ($sabbir) {
            
            return redirect()->route('all.teacher');
         }

         else{
            return redirect()->back('/');
         }

        
       


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {       
        $rajib=DB::table('teachers')->where('id',$id)->first();
        return view('admin.teacher.edit', compact('rajib'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $data=array();
       $data['firstName']=$request->firstName;
       $data['lastName']=$request->lastName;
       $data['email']=$request->email;
       $data['gender']=$request->gender;
       $data['city']=$request->city;

       $images=$request->file('image');

       if ($images) {
           $img_name=hexdec(uniqid());
         $ext=strtolower($images->getClientOriginalExtension());
         $img_full_name=$img_name.'.'.$ext;
         $location="upload/";
         $image_url=$location.$img_full_name;
         $success=$images->move($location,$img_full_name);

         $data['image']=$image_url;
         unlink($request->image_old);
         DB::table('teachers')->where('id',$id)->update($data);
         return redirect()->route('all.teacher');

       }
       else{
        $data['image']=$request->image_old;
        DB::table('teachers')->where('id',$id)->update($data);
        return redirect()->route('all.teacher');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $rian=DB::table('teachers')->where('id',$id)->first();
       $image=$rian->image;

       $delete=DB::table('teachers')->where('id',$id)->delete();
       if ($delete) {
        unlink( $image);
        return redirect()->route('all.teacher');
           # code...
       }
       else{
        return redirect()->route('all.teacher');
       }
    }
}
