@extends('admin.master.app')

@section('title')
    All Students
@endsection

@section('content')

    <div class="container-fluid">
                        <h1 class="mt-4">Tables</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Tables</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                               
                            </div>
                        </div>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                DataTable Example
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>email</th>
                                                <th>Hobby</th>
                                                <th>Image</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                       
                                        <tbody>
                                           @foreach($rajib as $rian)

                                          
                                            <tr>
                                                <td>{{$rian->id}}</td>
                                                <td>{{$rian->firstName}}</td>
                                                <td>{{$rian->lastName}}</td>
                                                <td>{{$rian->email}}</td>
                                                <td>{{$rian->hobby}}</td>
                                                <td><img src="{{$rian->image}}"width="200px" height="100px"></td>
                                                <td>
                                                     <a class="btn btn-warning" href="{{url('/teacher-edit/'.$rian->id)}}">Edit</a>
                                                     
                                                    <a class="btn btn-danger" href="{{url('/teacher-delete/'.$rian->id)}}">Delete</a> 

                                                </td>
                                                
                                            </tr>
                                         
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection