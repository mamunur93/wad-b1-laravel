@extends('admin.master.app')
@section('title')
    Create Student
@endsection
@section('content')
    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Create Account</h3></div>
                                    <div class="card-body">
                                        <form action="{{url('teacher-update/'.$rajib->id)}}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputFirstName">First Name</label>
                                                        <input class="form-control py-4" id="inputFirstName" type="text" value="{{$rajib->firstName}}" name="firstName" />
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputLastName">Last Name</label>
                                                        <input class="form-control py-4" id="inputLastName" type="text" value="{{$rajib->lastName}}" name="lastName" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Email</label>
                                                <input class="form-control py-4" id="inputEmailAddress" type="email" aria-describedby="emailHelp" value="{{$rajib->email}}" name="email" />
                                            </div>

                                             <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Gender</label>
                                                
                                               		<div class="form-check">
													  	<input class="form-check-input" type="radio" name="gender" id="Male" value="Male"  @if($rajib->gender =="Male")
                                                        {{"checked" }} @endif >
													 
													  <label class="form-check-label" for="Male">
													   Male
													  </label>

													</div>
													<div class="form-check">
													  
													  <input class="form-check-input" type="radio" name="gender" id="female" value="Female" @if($rajib->gender=="Female"){ { "checked"}} @endif >
													  
													  <label class="form-check-label" for="female">
													  Female
													  </label>
													</div>	
                                            </div>

                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">City</label>
                                               <select class="form-control" name="city">
                                                  <option value="{{$rajib->city}}">{{$rajib->city}}</option>
												  <option value="Chittagong">Chittagong</option>
												  <option value="Dhaka">Dhaka</option>
												  <option value="Khulna">Khulna</option>
												  <option value="Sylhet">Sylhet</option>
												</select>
                                            </div>

                                              

                                             <div class="form-group">
											    <label for="exampleFormControlFile1">Profile Picture</label>
                                                <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">


											    <input type="hidden" name="image_old" class="form-control-file" id="exampleFormControlFile1" value="{{$rajib->image}}">

                                                <img src="{{url($rajib->image)}}" height="100px" width="200px">
											  </div>




                                            
                                            <div class="form-group mt-4 mb-0"><button class="btn btn-primary btn-block">submit</button></div>
                                        </form>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
@endsection