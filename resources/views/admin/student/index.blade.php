@extends('admin.master.app')
@section('title')
    Dashboard
@endsection
@section('content')
    <div class="container-fluid">
                        <h1 class="mt-4">Dashboard</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Table Join</li>
                            <li class="breadcrumb-item active"><a href="{{url('/pdf')}}" class="btn btn-info">Downlaod PDF</a></li>
                        </ol>

                        
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Student Join
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>email</th>
                                                <th>Registation number</th>
                                               
                                            </tr>
                                        </thead>
                                       
                                        <tbody>
                                             @foreach($rajib as $row)   
                                            <tr>
                                               <th>{{$row->firstName}}  {{$row->lastName}}</th>
                                               <th>{{$row->email}} </th>
                                               <th>{{$row->reg}}</th>
                                            </tr>
                                            @endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection