@extends('admin.master.app')
@section('title')
    Create Student
@endsection
@section('content')
    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Create Account</h3></div>

                                    @if(Session::has('success'))
                                        <div class="card-header">
                                            <div class="alert alert-success">
                                                {{Session::get('success')}}
                                            </div>
                                        </div>
                                    @endif

                                     @if(Session::has('failed'))
                                        <div class="card-header">
                                            <div class="alert alert-danger">
                                                {{Session::get('failed')}}
                                            </div>
                                        </div>
                                    @endif
                                    <div class="card-body">
                                        <form action="{{url('/info-reg')}}" method="post">
                                            @csrf
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputFirstName">First Name</label>
                                                        <input class="form-control py-4" id="inputFirstName" type="text" placeholder="Enter first name" name="firstName" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputLastName">Last Name</label>
                                                        <input class="form-control py-4" id="inputLastName" type="text" placeholder="Enter last name" name="lastName" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Email</label>
                                                <input class="form-control py-4" id="inputEmailAddress" type="email" aria-describedby="emailHelp" placeholder="Enter email address" name="email" />
                                            </div>
                                            
                                            <div class="form-group mt-4 mb-0"><button class="btn btn-primary btn-block">submit</button></div>
                                        </form>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
@endsection