<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.index');
})->name('index');

// Route::get('/portfolio', function(){
// 	return view('template.portfolio');
// });

// Route::get('/about', function(){
// 	return view('template.about');
// });

// Route::get('/ankon', function(){
// 	return view('template.contact-us');
// });

//Students
Route::get('/create','StudentController@create')->name('create');

Route::get('/all-students', 'StudentController@index')->name('all');
Route::post('/store','StudentController@store');
Route::get('/delete/{id}','StudentController@destroy')->name('student.delete');
Route::get('/edit/{id}','StudentController@edit');
Route::post('/update/{id}','StudentController@update');


//Teacher

Route::get('/create-teacher','TeacherController@create')->name('create.teacher');
Route::post('/teacher-store','TeacherController@store');
Route::get('/all-teacher', 'TeacherController@index')->name('all.teacher');
Route::get('/teacher-delete/{id}','TeacherController@destroy');

Route::get('/teacher-edit/{id}','TeacherController@edit');
Route::post('/teacher-update/{id}', 'TeacherController@update');

//table join
Route::get('/info', 'informationController@create')->name('info');
Route::post('/info-reg', 'informationController@store');


Route::get('/reg', 'informationController@registation')->name('reg');
Route::post('/reg-store', 'informationController@regstore');

Route::get('/join', 'informationController@index');

Route::get('/pdf', 'informationController@pdf');









Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*
========
admin
=======
*/


Route::group(['middleware'=>['auth','admin']], function(){
	
Route::get('admin/dashboard','Admin\DashboardController@index')->name('admin.dashboard');
});


/*
========
author
=======
*/


Route::group(['middleware'=>['auth','author']], function(){

	Route::get('author/dashboard','Author\DashboardController@index')->name('author.dashboard');

});
