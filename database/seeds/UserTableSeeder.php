<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
         	'role_id'=>'1',
            'name' => 'Mr.Admin',
            'email'=>'sabbir@gmail.com',
            'password' => Hash::make('password'),
        ]);

         DB::table('users')->insert([
         	'role_id'=>'2',
            'name' => 'Mr.author',
            'email'=>'autrhor@gmail.com',
            'password' => Hash::make('password'),
        ]);


       

    }
}
